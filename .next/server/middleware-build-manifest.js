self.__BUILD_MANIFEST = {
	polyfillFiles: ['static/chunks/polyfills.js'],
	devFiles: ['static/chunks/react-refresh.js'],
	ampDevFiles: [],
	lowPriorityFiles: ['static/development/_buildManifest.js', 'static/development/_ssgManifest.js'],
	rootMainFiles: [],
	pages: {
		'/': ['static/chunks/webpack.js', 'static/chunks/main.js', 'static/chunks/pages/index.js'],
		'/_app': ['static/chunks/webpack.js', 'static/chunks/main.js', 'static/chunks/pages/_app.js'],
		'/_error': [
			'static/chunks/webpack.js',
			'static/chunks/main.js',
			'static/chunks/pages/_error.js',
		],
		'/geo': ['static/chunks/webpack.js', 'static/chunks/main.js', 'static/chunks/pages/geo.js'],
		'/swagger': [
			'static/chunks/webpack.js',
			'static/chunks/main.js',
			'static/chunks/pages/swagger.js',
		],
	},
	ampFirstPages: [],
};
