'use strict';
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
	var exports = {};
	exports.id = 'pages/api/swagger';
	exports.ids = ['pages/api/swagger'];
	exports.modules = {
		/***/ 'next/dist/compiled/next-server/pages-api.runtime.dev.js':
			/*!**************************************************************************!*\
  !*** external "next/dist/compiled/next-server/pages-api.runtime.dev.js" ***!
  \**************************************************************************/
			/***/ module => {
				module.exports = require('next/dist/compiled/next-server/pages-api.runtime.dev.js');

				/***/
			},

		/***/ 'js-yaml':
			/*!**************************!*\
  !*** external "js-yaml" ***!
  \**************************/
			/***/ module => {
				module.exports = import('js-yaml');

				/***/
			},

		/***/ fs:
			/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
			/***/ module => {
				module.exports = require('fs');

				/***/
			},

		/***/ path:
			/*!***********************!*\
  !*** external "path" ***!
  \***********************/
			/***/ module => {
				module.exports = require('path');

				/***/
			},

		/***/ '(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fswagger&preferredRegion=&absolutePagePath=.%2Fsrc%2Fpages%2Fapi%2Fswagger.ts&middlewareConfigBase64=e30%3D!':
			/*!**************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fswagger&preferredRegion=&absolutePagePath=.%2Fsrc%2Fpages%2Fapi%2Fswagger.ts&middlewareConfigBase64=e30%3D! ***!
  \**************************************************************************************************************************************************************************************************************************/
			/***/ (module, __webpack_exports__, __webpack_require__) => {
				eval(
					'__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   config: () => (/* binding */ config),\n/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__),\n/* harmony export */   routeModule: () => (/* binding */ routeModule)\n/* harmony export */ });\n/* harmony import */ var next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! next/dist/server/future/route-modules/pages-api/module.compiled */ "(api)/./node_modules/next/dist/server/future/route-modules/pages-api/module.compiled.js");\n/* harmony import */ var next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/dist/server/future/route-kind */ "(api)/./node_modules/next/dist/server/future/route-kind.js");\n/* harmony import */ var next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/dist/build/templates/helpers */ "(api)/./node_modules/next/dist/build/templates/helpers.js");\n/* harmony import */ var _src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./src/pages/api/swagger.ts */ "(api)/./src/pages/api/swagger.ts");\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([_src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__]);\n_src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\n// Import the userland code.\n\n// Re-export the handler (should be the default export).\n/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ((0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__, "default"));\n// Re-export config.\nconst config = (0,next_dist_build_templates_helpers__WEBPACK_IMPORTED_MODULE_2__.hoist)(_src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__, "config");\n// Create and export the route module that will be consumed.\nconst routeModule = new next_dist_server_future_route_modules_pages_api_module_compiled__WEBPACK_IMPORTED_MODULE_0__.PagesAPIRouteModule({\n    definition: {\n        kind: next_dist_server_future_route_kind__WEBPACK_IMPORTED_MODULE_1__.RouteKind.PAGES_API,\n        page: "/api/swagger",\n        pathname: "/api/swagger",\n        // The following aren\'t used in production.\n        bundlePath: "",\n        filename: ""\n    },\n    userland: _src_pages_api_swagger_ts__WEBPACK_IMPORTED_MODULE_3__\n});\n\n//# sourceMappingURL=pages-api.js.map\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9ub2RlX21vZHVsZXMvbmV4dC9kaXN0L2J1aWxkL3dlYnBhY2svbG9hZGVycy9uZXh0LXJvdXRlLWxvYWRlci9pbmRleC5qcz9raW5kPVBBR0VTX0FQSSZwYWdlPSUyRmFwaSUyRnN3YWdnZXImcHJlZmVycmVkUmVnaW9uPSZhYnNvbHV0ZVBhZ2VQYXRoPS4lMkZzcmMlMkZwYWdlcyUyRmFwaSUyRnN3YWdnZXIudHMmbWlkZGxld2FyZUNvbmZpZ0Jhc2U2ND1lMzAlM0QhIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7O0FBQXNHO0FBQ3ZDO0FBQ0w7QUFDMUQ7QUFDdUQ7QUFDdkQ7QUFDQSxpRUFBZSx3RUFBSyxDQUFDLHNEQUFRLFlBQVksRUFBQztBQUMxQztBQUNPLGVBQWUsd0VBQUssQ0FBQyxzREFBUTtBQUNwQztBQUNPLHdCQUF3QixnSEFBbUI7QUFDbEQ7QUFDQSxjQUFjLHlFQUFTO0FBQ3ZCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxLQUFLO0FBQ0wsWUFBWTtBQUNaLENBQUM7O0FBRUQscUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9nZW9kYXRhLXZpZXcvP2U0ZWEiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgUGFnZXNBUElSb3V0ZU1vZHVsZSB9IGZyb20gXCJuZXh0L2Rpc3Qvc2VydmVyL2Z1dHVyZS9yb3V0ZS1tb2R1bGVzL3BhZ2VzLWFwaS9tb2R1bGUuY29tcGlsZWRcIjtcbmltcG9ydCB7IFJvdXRlS2luZCB9IGZyb20gXCJuZXh0L2Rpc3Qvc2VydmVyL2Z1dHVyZS9yb3V0ZS1raW5kXCI7XG5pbXBvcnQgeyBob2lzdCB9IGZyb20gXCJuZXh0L2Rpc3QvYnVpbGQvdGVtcGxhdGVzL2hlbHBlcnNcIjtcbi8vIEltcG9ydCB0aGUgdXNlcmxhbmQgY29kZS5cbmltcG9ydCAqIGFzIHVzZXJsYW5kIGZyb20gXCIuL3NyYy9wYWdlcy9hcGkvc3dhZ2dlci50c1wiO1xuLy8gUmUtZXhwb3J0IHRoZSBoYW5kbGVyIChzaG91bGQgYmUgdGhlIGRlZmF1bHQgZXhwb3J0KS5cbmV4cG9ydCBkZWZhdWx0IGhvaXN0KHVzZXJsYW5kLCBcImRlZmF1bHRcIik7XG4vLyBSZS1leHBvcnQgY29uZmlnLlxuZXhwb3J0IGNvbnN0IGNvbmZpZyA9IGhvaXN0KHVzZXJsYW5kLCBcImNvbmZpZ1wiKTtcbi8vIENyZWF0ZSBhbmQgZXhwb3J0IHRoZSByb3V0ZSBtb2R1bGUgdGhhdCB3aWxsIGJlIGNvbnN1bWVkLlxuZXhwb3J0IGNvbnN0IHJvdXRlTW9kdWxlID0gbmV3IFBhZ2VzQVBJUm91dGVNb2R1bGUoe1xuICAgIGRlZmluaXRpb246IHtcbiAgICAgICAga2luZDogUm91dGVLaW5kLlBBR0VTX0FQSSxcbiAgICAgICAgcGFnZTogXCIvYXBpL3N3YWdnZXJcIixcbiAgICAgICAgcGF0aG5hbWU6IFwiL2FwaS9zd2FnZ2VyXCIsXG4gICAgICAgIC8vIFRoZSBmb2xsb3dpbmcgYXJlbid0IHVzZWQgaW4gcHJvZHVjdGlvbi5cbiAgICAgICAgYnVuZGxlUGF0aDogXCJcIixcbiAgICAgICAgZmlsZW5hbWU6IFwiXCJcbiAgICB9LFxuICAgIHVzZXJsYW5kXG59KTtcblxuLy8jIHNvdXJjZU1hcHBpbmdVUkw9cGFnZXMtYXBpLmpzLm1hcCJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fswagger&preferredRegion=&absolutePagePath=.%2Fsrc%2Fpages%2Fapi%2Fswagger.ts&middlewareConfigBase64=e30%3D!\n',
				);

				/***/
			},

		/***/ '(api)/./src/pages/api/swagger.ts':
			/*!**********************************!*\
  !*** ./src/pages/api/swagger.ts ***!
  \**********************************/
			/***/ (module, __webpack_exports__, __webpack_require__) => {
				eval(
					'__webpack_require__.a(module, async (__webpack_handle_async_dependencies__, __webpack_async_result__) => { try {\n__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   "default": () => (/* binding */ swaggerHandler)\n/* harmony export */ });\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! fs */ "fs");\n/* harmony import */ var fs__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(fs__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var js_yaml__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! js-yaml */ "js-yaml");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! path */ "path");\n/* harmony import */ var path__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(path__WEBPACK_IMPORTED_MODULE_2__);\nvar __webpack_async_dependencies__ = __webpack_handle_async_dependencies__([js_yaml__WEBPACK_IMPORTED_MODULE_1__]);\njs_yaml__WEBPACK_IMPORTED_MODULE_1__ = (__webpack_async_dependencies__.then ? (await __webpack_async_dependencies__)() : __webpack_async_dependencies__)[0];\n\n\n\nconst swaggerDefinitionPath = path__WEBPACK_IMPORTED_MODULE_2___default().join(process.cwd(), "public", "data", "swagger.yaml");\nfunction swaggerHandler(req, res) {\n    try {\n        const swaggerDefinition = js_yaml__WEBPACK_IMPORTED_MODULE_1__["default"].load(fs__WEBPACK_IMPORTED_MODULE_0___default().readFileSync(swaggerDefinitionPath, "utf8"));\n        res.status(200).json(swaggerDefinition);\n    } catch (error) {\n        console.error("Error loading Swagger definition:", error);\n        res.status(500).json({\n            message: "Error parsing YAML"\n        });\n    }\n}\n\n__webpack_async_result__();\n} catch(e) { __webpack_async_result__(e); } });//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiKGFwaSkvLi9zcmMvcGFnZXMvYXBpL3N3YWdnZXIudHMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQW9CO0FBQ087QUFFSDtBQUV4QixNQUFNRyx3QkFBd0JELGdEQUFTLENBQUNHLFFBQVFDLEdBQUcsSUFBSSxVQUFVLFFBQVE7QUFFMUQsU0FBU0MsZUFBZUMsR0FBbUIsRUFBRUMsR0FBb0I7SUFDL0UsSUFBSTtRQUNILE1BQU1DLG9CQUFvQlQsb0RBQVMsQ0FBQ0Qsc0RBQWUsQ0FBQ0csdUJBQXVCO1FBQzNFTSxJQUFJSSxNQUFNLENBQUMsS0FBS0MsSUFBSSxDQUFDSjtJQUN0QixFQUFFLE9BQU9LLE9BQU87UUFDZkMsUUFBUUQsS0FBSyxDQUFDLHFDQUFxQ0E7UUFDbkROLElBQUlJLE1BQU0sQ0FBQyxLQUFLQyxJQUFJLENBQUM7WUFBQ0csU0FBUztRQUFvQjtJQUNwRDtBQUNEIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vZ2VvZGF0YS12aWV3Ly4vc3JjL3BhZ2VzL2FwaS9zd2FnZ2VyLnRzP2JjMjMiXSwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IGZzIGZyb20gJ2ZzJztcbmltcG9ydCB5YW1sIGZyb20gJ2pzLXlhbWwnO1xuaW1wb3J0IHtOZXh0QXBpUmVxdWVzdCwgTmV4dEFwaVJlc3BvbnNlfSBmcm9tICduZXh0JztcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnO1xuXG5jb25zdCBzd2FnZ2VyRGVmaW5pdGlvblBhdGggPSBwYXRoLmpvaW4ocHJvY2Vzcy5jd2QoKSwgJ3B1YmxpYycsICdkYXRhJywgJ3N3YWdnZXIueWFtbCcpO1xuXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbiBzd2FnZ2VySGFuZGxlcihyZXE6IE5leHRBcGlSZXF1ZXN0LCByZXM6IE5leHRBcGlSZXNwb25zZSkge1xuXHR0cnkge1xuXHRcdGNvbnN0IHN3YWdnZXJEZWZpbml0aW9uID0geWFtbC5sb2FkKGZzLnJlYWRGaWxlU3luYyhzd2FnZ2VyRGVmaW5pdGlvblBhdGgsICd1dGY4JykpO1xuXHRcdHJlcy5zdGF0dXMoMjAwKS5qc29uKHN3YWdnZXJEZWZpbml0aW9uKTtcblx0fSBjYXRjaCAoZXJyb3IpIHtcblx0XHRjb25zb2xlLmVycm9yKCdFcnJvciBsb2FkaW5nIFN3YWdnZXIgZGVmaW5pdGlvbjonLCBlcnJvcik7XG5cdFx0cmVzLnN0YXR1cyg1MDApLmpzb24oe21lc3NhZ2U6ICdFcnJvciBwYXJzaW5nIFlBTUwnfSk7XG5cdH1cbn1cbiJdLCJuYW1lcyI6WyJmcyIsInlhbWwiLCJwYXRoIiwic3dhZ2dlckRlZmluaXRpb25QYXRoIiwiam9pbiIsInByb2Nlc3MiLCJjd2QiLCJzd2FnZ2VySGFuZGxlciIsInJlcSIsInJlcyIsInN3YWdnZXJEZWZpbml0aW9uIiwibG9hZCIsInJlYWRGaWxlU3luYyIsInN0YXR1cyIsImpzb24iLCJlcnJvciIsImNvbnNvbGUiLCJtZXNzYWdlIl0sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///(api)/./src/pages/api/swagger.ts\n',
				);

				/***/
			},
	};
	// load runtime
	var __webpack_require__ = require('../../webpack-api-runtime.js');
	__webpack_require__.C(exports);
	var __webpack_exec__ = moduleId => __webpack_require__((__webpack_require__.s = moduleId));
	var __webpack_exports__ = __webpack_require__.X(0, ['vendor-chunks/next'], () =>
		__webpack_exec__(
			'(api)/./node_modules/next/dist/build/webpack/loaders/next-route-loader/index.js?kind=PAGES_API&page=%2Fapi%2Fswagger&preferredRegion=&absolutePagePath=.%2Fsrc%2Fpages%2Fapi%2Fswagger.ts&middlewareConfigBase64=e30%3D!',
		),
	);
	module.exports = __webpack_exports__;
})();
