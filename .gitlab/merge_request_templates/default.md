## Detailed description of changes

...

## Recommendations on how to test the changes

...

## Checklist for developer

- [ ] The code builds clean without any errors or warnings
- [ ] The code does not contain commented out code
- [ ] The code does not log anything to console
- [ ] I have added unit test(s) (where possible) to cover new code and successfully executed ran it
- [ ] I have thoroughly tested the new code and any adjacent features it may affect
- [ ] I made the code(s) sufficiently "readable"

## Screenshots or videos (before and after if appropriate)

#### Before

...

#### After

...
