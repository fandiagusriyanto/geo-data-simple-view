import {FormLogin} from '@/components';
import {useLogin} from '@/hooks';

const LoginPage = () => {
	const {errorMessage, handleLogin} = useLogin();

	return (
		<div className="flex min-h-screen items-center justify-center bg-gray-50 px-4 py-12 sm:px-6 lg:px-8">
			<div className="w-full max-w-md space-y-8">
				<div>
					<h2 className="mt-6 text-center text-3xl font-extrabold text-gray-900">
						Sign in to your account
					</h2>
					{errorMessage && <p className="mt-2 text-center text-red-600">{errorMessage}</p>}
				</div>
				<FormLogin onSubmit={handleLogin} />
			</div>
		</div>
	);
};

export default LoginPage;
