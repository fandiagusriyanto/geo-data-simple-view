import crypto from 'crypto';
import fs from 'fs';
import jwt from 'jsonwebtoken';
import {NextApiRequest, NextApiResponse} from 'next';
import path from 'path';

const usersFilePath = path.join(process.cwd(), 'public', 'data', 'users.json');

type User = {
	username: string;
	password: string;
};

const generateSecretKey = (): string => {
	return crypto.randomBytes(64).toString('hex');
};

export default function loginHandler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method === 'POST') {
		try {
			const {username, password} = req.body;
			const usersData = JSON.parse(fs.readFileSync(usersFilePath, 'utf-8')) as Array<User>;

			const user = usersData.find(data => data.username === username && data.password === password);

			if (user) {
				const secretKey = generateSecretKey();
				const token = jwt.sign({username}, secretKey, {expiresIn: '1h'});
				res.status(200).json({token});
			} else {
				res.status(401).json({message: 'Username or password is incorrect'});
			}
		} catch (error) {
			res.status(500).json({message: 'Internal server error'});
		}
	} else {
		res.status(405).end();
	}
}
