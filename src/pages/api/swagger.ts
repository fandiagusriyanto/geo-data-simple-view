import fs from 'fs';
import yaml from 'js-yaml';
import {NextApiRequest, NextApiResponse} from 'next';
import path from 'path';

const swaggerDefinitionPath = path.join(process.cwd(), 'public', 'data', 'swagger.yaml');

export default function swaggerHandler(req: NextApiRequest, res: NextApiResponse) {
	try {
		const swaggerDefinition = yaml.load(fs.readFileSync(swaggerDefinitionPath, 'utf8'));
		res.status(200).json(swaggerDefinition);
	} catch (error) {
		console.error('Error loading Swagger definition:', error);
		res.status(500).json({message: 'Error parsing YAML'});
	}
}
