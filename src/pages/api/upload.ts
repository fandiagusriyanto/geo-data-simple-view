import GeoJSONSchema from '@/utils/schema';
import fs from 'fs';
import multer from 'multer';
import {NextApiRequest, NextApiResponse} from 'next';
import path from 'path';

const UPLOAD_DIR = '/tmp';

const storage = multer.diskStorage({
	destination: UPLOAD_DIR,
	filename: function (req, file, cb) {
		cb(null, file.originalname);
	},
});

const upload = multer({
	storage,
	fileFilter: function (req, file, cb) {
		const extname = path.extname(file.originalname);
		if (extname.toLowerCase() !== '.geojson')
			return cb(new Error('Only .geojson files are allowed'));
		cb(null, true);
	},
});

export const config = {
	api: {
		bodyParser: false,
	},
};

export default async function uploadHandler(req: NextApiRequest, res: NextApiResponse) {
	if (req.method === 'POST') {
		try {
			upload.single('file')(req as any, res as any, async function (error: any) {
				if (error instanceof multer.MulterError) {
					console.error('Multer error:', error);
					return res.status(400).json({message: 'File upload error. Please try again.'});
				} else if (error) {
					console.error('Error uploading file:', error);
					return res.status(500).json({message: 'Error uploading file. Please try again.'});
				}

				const {file} = req as any;

				if (!file) return res.status(400).json({message: 'No file uploaded.'});

				const oldFilePath = file.path;
				const newFilePath = path.join(UPLOAD_DIR, file.originalname);
				fs.renameSync(oldFilePath, newFilePath);

				try {
					const jsonData = JSON.parse(fs.readFileSync(newFilePath, 'utf-8'));
					GeoJSONSchema.parse(jsonData);
					return res.status(200).json({
						message: 'File uploaded successfully',
						filename: file.originalname,
					});
				} catch (jsonError: any) {
					// Explicitly type jsonError as any
					console.error('Error parsing JSON data:', jsonError);
					const serializedError = {
						message: 'Error parsing JSON data.',
						details: jsonError.message,
					};
					return res.status(400).json(serializedError);
				}
			});
		} catch (error) {
			console.error('Error uploading files:', error);
			return res.status(500).json({message: 'Error uploading files. Please try again.'});
		}
	} else {
		return res.status(405).json({message: 'Method Not Allowed'});
	}
}
