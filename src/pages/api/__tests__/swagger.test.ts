import fs from 'fs';
import yaml from 'js-yaml';
import {NextApiRequest, NextApiResponse} from 'next';
import swaggerHandler from '../swagger';

jest.mock('fs', () => ({
	readFileSync: jest.fn(),
}));

jest.mock('js-yaml', () => ({
	load: jest.fn(),
}));

describe('Swagger Handler', () => {
	let req: Partial<NextApiRequest>;
	let res: Partial<NextApiResponse>;

	beforeEach(() => {
		req = {};
		res = {
			status: jest.fn().mockReturnThis(),
			json: jest.fn(),
		};
	});

	it('should return 200 and Swagger definition', () => {
		const mockSwaggerDefinition = {swagger: 'definition'};
		(fs.readFileSync as jest.Mock).mockReturnValue('swagger definition content');
		(yaml.load as jest.Mock).mockReturnValue(mockSwaggerDefinition);

		swaggerHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(200);
		expect(res.json).toHaveBeenCalledWith(mockSwaggerDefinition);
	});

	it('should return 500 and error message if yaml parsing fails', () => {
		const errorMessage = 'Error parsing YAML';
		(fs.readFileSync as jest.Mock).mockReturnValue('swagger definition content');
		(yaml.load as jest.Mock).mockImplementation(() => {
			throw new Error(errorMessage);
		});

		swaggerHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(500);
		expect(res.json).toHaveBeenCalledWith({message: errorMessage});
	});
});
