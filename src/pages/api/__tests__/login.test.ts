import loginHandler from '@/pages/api/login';
import fs from 'fs';
import jwt from 'jsonwebtoken';
import {NextApiRequest, NextApiResponse} from 'next';

jest.mock('fs', () => ({
	readFileSync: jest.fn(),
}));

describe('API Handler', () => {
	let req: Partial<NextApiRequest>;
	let res: Partial<NextApiResponse>;

	beforeEach(() => {
		req = {
			method: 'POST',
			body: {
				username: 'testuser',
				password: 'testpassword',
			},
		};
		res = {
			status: jest.fn().mockReturnThis(),
			json: jest.fn(),
			end: jest.fn(),
		};
	});

	it('should return a token if username and password are correct', () => {
		const usersData = [{username: 'testuser', password: 'testpassword'}];
		(fs.readFileSync as jest.Mock).mockReturnValue(JSON.stringify(usersData));
		const mockToken = 'mockToken' as any;
		const mockSign = jest.spyOn(jwt, 'sign').mockReturnValue(mockToken);

		loginHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(200);
		expect(res.json).toHaveBeenCalledWith({token: mockToken});
		expect(mockSign).toHaveBeenCalled();
	});

	it('should return 401 if username or password is incorrect', () => {
		const usersData = [{username: 'testuser', password: 'testpassword'}];
		(fs.readFileSync as jest.Mock).mockReturnValue(JSON.stringify(usersData));
		req.body = {username: 'wronguser', password: 'wrongpassword'};

		loginHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(401);
		expect(res.json).toHaveBeenCalledWith({message: 'Username or password is incorrect'});
	});

	it('should return 500 if an error occurs while reading file', () => {
		(fs.readFileSync as jest.Mock).mockImplementation(() => {
			throw new Error('Mock Error');
		});

		loginHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(500);
		expect(res.json).toHaveBeenCalledWith({message: 'Internal server error'});
	});

	it('should return 405 if request method is not POST', () => {
		req.method = 'GET';

		loginHandler(req as NextApiRequest, res as NextApiResponse);

		expect(res.status).toHaveBeenCalledWith(405);
		expect(res.end).toHaveBeenCalled();
	});
});
