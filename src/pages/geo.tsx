import {MapVisualization, UploadFile} from '@/components';
import {useRedirect, useUploadFile} from '@/hooks/';
import router from 'next/router';
import {useEffect} from 'react';
import {FaPowerOff} from 'react-icons/fa';

const GeoPage = () => {
	useEffect(() => {
		const token = localStorage.getItem('token');

		if (!token) router.push('/');
	}, []);

	const {isUploaded, data, handleFileUpload} = useUploadFile();

	const {handleDocumentationClick, handleGeoClick, handleLogout} = useRedirect();

	return (
		<div className="flex min-h-screen flex-col bg-gray-100">
			<div className="flex items-center justify-between bg-blue-800 px-4 py-2 text-white">
				<div className="flex items-center space-x-4">
					<h3 className="cursor-pointer text-xl font-bold" onClick={handleGeoClick}>
						GeoData Simple View
					</h3>
					<span className="text-base font-medium"> | </span>
					<span className="cursor-pointer text-base font-medium" onClick={handleDocumentationClick}>
						API Documentation
					</span>
				</div>
				<div className="flex items-center space-x-2">
					<FaPowerOff size={24} />
					<button className="text-base font-medium" onClick={handleLogout}>
						Logout
					</button>
				</div>
			</div>
			<div className="flex flex-1 items-center justify-center">
				<div className="flex w-full max-w-xl flex-col items-center rounded-lg bg-white p-8 shadow-md">
					<UploadFile onUpload={handleFileUpload} />
					{isUploaded && <MapVisualization data={data} />}
				</div>
			</div>
		</div>
	);
};

export default GeoPage;
