import 'swagger-ui-react/swagger-ui.css';
import {useRedirect} from '@/hooks/';
import axios from 'axios';
import Head from 'next/head';
import {useState, useEffect} from 'react';
import {FaPowerOff} from 'react-icons/fa';
import SwaggerUI from 'swagger-ui-react';

const Swagger = () => {
	const [swaggerDocument, setSwaggerDocument] = useState(null);

	const {handleDocumentationClick, handleGeoClick, handleLogout} = useRedirect();

	useEffect(() => {
		const fetchSwaggerDocument = async () => {
			try {
				const response = await axios.get('/api/swagger');
				if (response.status === 200) {
					setSwaggerDocument(response.data);
				} else {
					console.error('Failed to fetch Swagger document:', response.statusText);
				}
			} catch (error) {
				console.error('Failed to fetch Swagger document:', error);
			}
		};

		fetchSwaggerDocument();
	}, []);

	return (
		<div className="flex min-h-screen flex-col bg-gray-100">
			<div className="flex items-center justify-between bg-blue-800 px-4 py-2 text-white">
				<div className="flex items-center space-x-4">
					<h3 className="cursor-pointer text-xl font-bold" onClick={handleGeoClick}>
						GeoData Simple View
					</h3>
					<span className="text-base font-medium"> | </span>
					<span className="cursor-pointer text-base font-medium" onClick={handleDocumentationClick}>
						API Documentation
					</span>
				</div>
				<div className="flex items-center space-x-2">
					<FaPowerOff size={24} />
					<button className="text-base font-medium" onClick={handleLogout}>
						Logout
					</button>
				</div>
			</div>
			<Head>
				<title>API Documentation</title>
			</Head>
			<div className="flex min-h-screen items-center justify-center bg-gray-100">
				<div className="w-full max-w-4xl">
					{swaggerDocument ? (
						<SwaggerUI spec={swaggerDocument} />
					) : (
						<p>Loading Swagger document...</p>
					)}
				</div>
			</div>
		</div>
	);
};

export default Swagger;
