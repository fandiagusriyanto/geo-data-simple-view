import axios, {AxiosError} from 'axios';
import {useState} from 'react';

type LoginResponse = {
	token: string;
	message: string;
};

export const useLogin = () => {
	const [errorMessage, setErrorMessage] = useState<string>('');

	const handleLogin = async ({username, password}: {username: string; password: string}) => {
		try {
			const response = await axios.post<LoginResponse>(
				'/api/login',
				{username, password},
				{
					headers: {
						'Content-Type': 'application/json',
					},
				},
			);

			if (response.status === 200) {
				const {token} = response.data;
				localStorage.setItem('token', token);
				window.location.href = '/geo';
			}
		} catch (error) {
			if (axios.isAxiosError(error)) {
				const axiosError = error as AxiosError<LoginResponse>;
				if (axiosError.response) {
					setErrorMessage(axiosError.response.data.message);
				} else {
					setErrorMessage('Failed to login. Please try again later.');
				}
			} else {
				setErrorMessage('Failed to login. Please try again later.');
			}
		}
	};

	return {errorMessage, handleLogin};
};
