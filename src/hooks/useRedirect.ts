import router from 'next/router';

export const useRedirect = () => {
	const handleDocumentationClick = () => router.push('/swagger');
	const handleGeoClick = () => router.push('/geo');
	const handleLogout = () => {
		localStorage.removeItem('token');
		router.push('/');
	};

	return {
		handleDocumentationClick,
		handleGeoClick,
		handleLogout,
	};
};
