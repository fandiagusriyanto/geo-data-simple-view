import axios from 'axios';
import {useState} from 'react';

export const useUploadFile = () => {
	const [isUploaded, setIsUploaded] = useState<boolean>(false);
	const [data, setData] = useState<Array<File>>([]);

	const handleFileUpload = (file: File) => {
		const formData = new FormData();
		formData.append('file', file);

		axios
			.post('/api/upload', formData)
			.then(response => {
				setData(response.data);
				setIsUploaded(true);
			})
			.catch(error => {
				if (error.response) {
					alert(error.response.data.message);
				} else if (error.request) {
					alert('No response received from the server');
				} else {
					alert('Error uploading file. Please try again.');
				}
			});
	};

	return {isUploaded, data, handleFileUpload};
};
