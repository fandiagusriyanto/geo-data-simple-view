import React from 'react';
import {useForm} from 'react-hook-form';
import {z} from 'zod';

type FormData = {
	file: FileList;
};

const schema = z.object({
	file: z.custom<Array<File>>(),
});

type UploadFileProps = {
	onUpload: (filename: File) => void;
};

export const UploadFile: React.FC<UploadFileProps> = ({onUpload}) => {
	const {
		register,
		handleSubmit,
		formState: {errors},
	} = useForm<FormData>({
		resolver: async data => {
			try {
				await schema.parseAsync(data);
				return {values: data, errors: {}};
			} catch (error: any) {
				return {values: {}, errors: error.errors || {_error: error.message}};
			}
		},
	});

	const onUploadSubmit = async ({file: [fileUploaded]}: FormData) => {
		if (fileUploaded.name.includes('.geojson')) {
			return onUpload(fileUploaded);
		}
		return alert('Error uploading file. Invalid file format');
	};

	return (
		<form onSubmit={handleSubmit(onUploadSubmit)} className="flex flex-col items-center gap-4">
			<label htmlFor="file" className="font-medium text-gray-800">
				Choose a file:
			</label>
			<div className="flex items-center space-x-2 text-black">
				<input
					id="file"
					type="file"
					{...register('file')}
					className="rounded-md border border-gray-300 p-2"
				/>
				<button
					type="submit"
					className="rounded-md bg-blue-500 px-4 py-2 text-white transition duration-300 ease-in-out hover:bg-blue-600"
				>
					Upload File
				</button>
			</div>
			{errors && <p className="text-red-500">{errors?.file?.message}</p>}
		</form>
	);
};
