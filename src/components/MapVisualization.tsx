import React, {useEffect, useRef} from 'react';

type MapVisualizationProps = {
	data: any;
};

export const MapVisualization: React.FC<MapVisualizationProps> = ({data}) => {
	const mapContainerRef = useRef<HTMLDivElement>(null);

	useEffect(() => {
		if (!data || !mapContainerRef.current) return;

		import('leaflet')
			.then(L => {
				const map = L.map(mapContainerRef.current!).setView([-2, 120], 5);

				L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
					attribution:
						'&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
					maxZoom: 18,
				}).addTo(map);

				// Function to handle each feature in the GeoJSON data
				function onEachFeature(feature: any, layer: any) {
					if (feature.properties && feature.properties.prop0) {
						layer.bindPopup(feature.properties.prop0);
					}
				}

				// Add GeoJSON data to the map
				L.geoJSON(data, {
					onEachFeature: onEachFeature,
				}).addTo(map);

				// Fit the map bounds to the GeoJSON data
				if (data.features && data.features.length > 0) {
					map.fitBounds(L.geoJSON(data).getBounds());
				}
			})
			.catch(error => {
				console.error('Error loading Leaflet:', error);
			});
	}, [data]);

	return (
		<div className="relative my-4 h-96 w-full overflow-hidden rounded-lg shadow-md">
			<div ref={mapContainerRef} className="h-full w-full" />
		</div>
	);
};
