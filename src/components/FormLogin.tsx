import {useState} from 'react';

export const FormLogin = ({
	onSubmit,
}: {
	onSubmit: (data: {username: string; password: string}) => void;
}) => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');

	const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		onSubmit({username, password});
	};

	return (
		<form onSubmit={handleSubmit} className="mb-4 rounded bg-white px-8 pb-8 pt-6 shadow-md">
			<div className="mb-4">
				<label className="mb-2 block text-sm font-bold text-gray-700" htmlFor="username">
					Username
				</label>
				<input
					className="focus:shadow-outline w-full appearance-none rounded border px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
					id="username"
					type="text"
					placeholder="Username"
					value={username}
					onChange={e => setUsername(e.target.value)}
				/>
			</div>
			<div className="mb-6">
				<label className="mb-2 block text-sm font-bold text-gray-700" htmlFor="password">
					Password
				</label>
				<input
					className="focus:shadow-outline mb-3 w-full appearance-none rounded border border-red-500 px-3 py-2 leading-tight text-gray-700 shadow focus:outline-none"
					id="password"
					type="password"
					placeholder="******************"
					value={password}
					onChange={e => setPassword(e.target.value)}
				/>
			</div>
			<div className="flex items-center justify-between">
				<button
					className="focus:shadow-outline rounded bg-blue-500 px-4 py-2 font-bold text-white hover:bg-blue-700 focus:outline-none"
					type="submit"
				>
					Sign In
				</button>
			</div>
		</form>
	);
};
