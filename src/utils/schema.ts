import {z} from 'zod';

const PointSchema = z.object({
	type: z.literal('Point'),
	coordinates: z.tuple([z.number(), z.number()]),
});

const LineStringSchema = z.object({
	type: z.literal('LineString'),
	coordinates: z.array(z.tuple([z.number(), z.number()])),
});

const PolygonSchema = z.object({
	type: z.literal('Polygon'),
	coordinates: z.array(z.array(z.tuple([z.number(), z.number()]))),
});

const MultiPointSchema = z.object({
	type: z.literal('MultiPoint'),
	coordinates: z.array(z.tuple([z.number(), z.number()])),
});

const MultiPolygonSchema = z.object({
	type: z.literal('MultiPolygon'),
	coordinates: z.array(z.array(z.array(z.tuple([z.number(), z.number()])))),
});

const Geometry = z.union([
	PointSchema,
	LineStringSchema,
	PolygonSchema,
	MultiPointSchema,
	MultiPolygonSchema,
]);

const PropertyValue = z.union([z.string(), z.number()]);

const Feature = z.object({
	type: z.literal('Feature'),
	geometry: Geometry,
	properties: z.record(PropertyValue),
});

const GeoJSONSchema = z.object({
	type: z.literal('FeatureCollection'),
	features: z.array(Feature),
});

export default GeoJSONSchema;
