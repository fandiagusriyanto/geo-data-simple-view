<h1 align="center">GeoData Simple View</h1>

## Table of contents

- [Features](#features)
- [Pre requisites](#pre-requisites)
- [How to run](#how-to-run)
- [Branch name](#branch-name)
- [Commit message](#commit-message)
- [Merge request title](#merge-request-title)
- [Scripts](#scripts)
- [Application stack](#application-stack)
- [Contributors](#contributors)
- [Previews](#previews)

## Features

1. Login

```
[
  {
    "username": "admin",
    "password": "password123"
  },
  {
    "username": "user",
    "password": "123456"
  }
]
```

2. Route guard. Can't access upload page without login first.
3. Validators. Can't upload files other than geojson files and only data with correct structure is accepted.
4. API documentation. Swagger documentation that can be tested live.
5. Automatically deploy when push to branch main.
6. Pre-commit check quality codes.
7. Bundle analyzer.
8. API unit testing.

## Pre Requisites

1. Install [Git](https://git-scm.com/downloads)
2. Install [NodeJs](https://nodejs.org/en/download/) >= v18.12.1
3. Install [Yarn](https://classic.yarnpkg.com/lang/en/docs/install/#windows-stable)
4. Install [VSCode](https://code.visualstudio.com/download)

## How to run

1. Clone repository

```
  git clone https://gitlab.com/fandiagusriyanto/geo-data-simple-view.git
```

2. Install dependencies

```
  yarn
```

3. Run application. (Make sure you've .env file on your cloned project before you execute the command bellow).

```
  yarn dev
```

Hit the application in a browser at http://localhost:3000/

## Branch name

We used [commitlint](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum) convention for branch names.
Examples:

- **fix/things-that-you-did**
- **build/things-that-you-did**
- **ci/things-that-you-did**
- **docs/things-that-you-did**
- **feat/things-that-you-did**
- **chore/things-that-you-did**
- **perf/things-that-you-did**
- **refactor/things-that-you-did**
- **revert/things-that-you-did**
- **style/things-that-you-did**
- **test/things-that-you-did**

## Commit message

We used [commitlint](https://github.com/conventional-changelog/commitlint/tree/master/@commitlint/config-conventional#type-enum) convention for commit messages, and [gitmoji](https://gitmoji.dev/) to flag the purpose of commit.
Examples:

- **fix: bugfix :bug:**
- **ci: fix CI build :green_heart:**
- **feat: new feature :sparkles:**
- **chore: update project structure :art:**
- **build: build something :construction_worker:**
- **docs: add codes documentation :memo:**
- **perf: remove dead codes :coffin:**
- **refactor: refactor auth guard :recycle:**
- **revert: revert last changes :rewind:**
- **style: update UI :lipstick:**
- **test: add dateFormatter unit testing :white_check_mark:**

## Merge request title

We used special format for merge request title like this, **type(feature): commit message :emoji:**.

Example: **feat(maps): commit message :sparkles:**

## Script

1. Check linter, prettier, deadcode, and typescript

```
yarn checker
```

2. Check bundles

```
yarn analyze
```

3. Check test coverage

```
yarn test:coverage
```

4. Format codes to follow lint and prettier config automatically

```
yarn formatter
```

## Application Stack

1. [NextJs](https://nextjs.org/). The React Framework for the Web.
2. [TailwindCSS](https://tailwindcss.com//). A utility-first CSS framework packed with classes like flex, pt-4, text-center and rotate-90 that can be composed to build any design, directly in your markup.
3. [Axios](<https://www.mysql.com/](https://axios-http.com/docs/intro)>). Promise based HTTP client for the browser and node.js.
4. [TypeScript](https://www.typescriptlang.org/). JavaScript with syntax for types.
5. [Jest](https://jestjs.io/). A delightful JavaScript Testing Framework with a focus on simplicity.
6. [LeafletJs](https://leafletjs.com/). An open-source JavaScript library for mobile-friendly interactive maps.
7. [Multer](https://www.npmjs.com/package/multer). A node.js middleware for handling multipart/form-data, which is primarily used for uploading files.
8. [jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken). An implementation of JSON Web Tokens.
9. [react-hook-form](An implementation of JSON Web Tokens.). Performant, flexible and extensible forms with easy-to-use validation.
10. [Zod](https://zod.dev/). TypeScript-first schema validation with static type inference.
11. [swagger-ui-react](https://www.npmjs.com/package/swagger-ui-react). A flavor of Swagger UI suitable for use in React applications.
12. [Husky](https://typicode.github.io/husky/). Modern native git hooks.
13. [crypto](https://nodejs.org/api/crypto.html#crypto). Cryptographic functionality that includes a set of wrappers for OpenSSL's hash, HMAC, cipher, decipher, sign, and verify functions.

## Contributors

- [Fandi A Riyanto](https://github.com/FandiAR)

## Previews

- [Live PreView](https://geo-data-simple-view.vercel.app/geo)
- [API Documentation](https://geo-data-simple-view.vercel.app/swagger)
